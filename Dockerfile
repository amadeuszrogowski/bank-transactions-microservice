FROM openjdk:11-jre-slim

COPY build/libs/microservice-2.0.0.jar .

EXPOSE 8080

CMD java -jar microservice-2.0.0.jar