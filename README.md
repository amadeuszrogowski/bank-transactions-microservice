### About
`Author: Amadeusz Rogowski`    
`Date of creation: November 2020`  
        
Web service which expose REST api for retrieving information about bank transactions.  
Bank transactions, customers and account types data are loaded on a startup from csv files, parsed and then persisted into mongodb.
    
The endpoint accepts two values as input: list of customer ids and list of account types.  
If any of these request parameters are missing or sent with value ALL, application interprets them as list of all customer ids/account types.  
In response, the user receives a list of transactions sorted in ascending order by an amount, and the type of bank account from which the transaction was performed, as well as the name and surname of the ordering party.
    
    
### Starting up application
To start up application just run in commandline
    __`docker-compose up`__
in your root directory.  
Server will listen on port 8080. In file __`apiRequests.http`__ you have GET requests, so you can run them in IDE to test REST API. 

### Sample usage
For given GET request: http://localhost:8080/api/transactions/information?customer_id=2,3&account_type=3

You should expect given respone:
```json
    [
        {
            "transactionDate": "2014-02-01T15:11:03",
            "transactionId": "786",
            "amount": 29.22,
            "accountTypeName": "currency account",
            "customerFirstName": "Marek",
            "customerSecondName": "Tarkowski"
        },
        {
            "transactionDate": "2017-12-16T08:31:21",
            "transactionId": "787",
            "amount": 99.11,
            "accountTypeName": "currency account",
            "customerFirstName": "Marek",
            "customerSecondName": "Tarkowski"
        },
        {
            "transactionDate": "2013-07-12T13:27:18",
            "transactionId": "778",
            "amount": 199.11,
            "accountTypeName": "currency account",
            "customerFirstName": "Adrianna",
            "customerSecondName": "Nowak"
        },
        {
            "transactionDate": "2013-08-04T22:07:38",
            "transactionId": "781",
            "amount": 999.99,
            "accountTypeName": "currency account",
            "customerFirstName": "Marek",
            "customerSecondName": "Tarkowski"
        }
    ]
```

### Running Tests
You can run unit tests by running command: __`gradle clean test -i`__ in checkout directory.

### Additional comments
List of users for request authentication, is defined in __`application.properties`__  
Docker image of this application is available as __`amicolon/microservice`__ on docker hub.

### Technology stack
![kotlin logo](/image/kotlin.png)
![spring logo](/image/spring.png)
![java logo](/image/java.jpg)
![docker logo](/image/docker.jpg)
![mongodb logo](image/mongo.png)
![gradle logo](image/gradle.png)