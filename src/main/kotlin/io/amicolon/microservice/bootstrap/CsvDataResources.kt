package io.amicolon.microservice.bootstrap

import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import java.io.InputStream


@Configuration
class CsvDataResources {

    @Bean(name = ["transactions"])
    fun transactions() = getInputStream("transactions.csv")

    @Bean(name = ["customers"])
    fun customers() = getInputStream("customers.csv")

    @Bean(name = ["accounttypes"])
    fun accountTypes() = getInputStream("accounttypes.csv")

    private fun getInputStream(resourceName: String): InputStream = javaClass.getResourceAsStream("/$resourceName")
}