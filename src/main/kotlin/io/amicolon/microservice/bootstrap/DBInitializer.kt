package io.amicolon.microservice.bootstrap

import io.amicolon.microservice.factories.ObjectsFromCsvFactory
import io.amicolon.microservice.repositories.TransactionRepository
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.ApplicationArguments
import org.springframework.boot.ApplicationRunner
import org.springframework.stereotype.Component

@Component
class DBInitializer @Autowired constructor(
        private val factory: ObjectsFromCsvFactory,
        private val transactionRepository: TransactionRepository
) : ApplicationRunner {

    override fun run(args: ApplicationArguments?) {
        transactionRepository.deleteAll()

        val transactions = factory.getTransactions()
        transactionRepository.saveAll(transactions)
    }
}
