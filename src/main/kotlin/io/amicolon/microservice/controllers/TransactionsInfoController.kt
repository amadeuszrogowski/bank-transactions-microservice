package io.amicolon.microservice.controllers

import io.amicolon.microservice.domain.TransactionInfo
import io.amicolon.microservice.services.TransactionsInfoService
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.ResponseEntity
import org.springframework.stereotype.Controller
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RequestParam

@Controller
@RequestMapping("/api/transactions")
class TransactionsInfoController @Autowired constructor(
        private val transactionsInfoService: TransactionsInfoService
) {

    @GetMapping("/information")
    fun getBankCustomersInformation(
            @RequestParam(required = false, name = "customer_id") customerIds: List<String>?,
            @RequestParam(required = false, name = "account_type") accountTypesIds: List<String>?
    ): ResponseEntity<Any> {

        val transactionsInfo = transactionsInfoService.obtainTransactionsInformation(customerIds, accountTypesIds)

        return ok(transactionsInfo)
    }

    fun ok(transactionsInfo: List<TransactionInfo>): ResponseEntity<Any> = ResponseEntity.ok().body(transactionsInfo)
}