package io.amicolon.microservice.domain

import org.springframework.data.annotation.Id
import org.springframework.data.mongodb.core.mapping.Document
import java.math.BigDecimal
import java.time.LocalDateTime

@Document(collection = "transactions")
data class Transaction(
        @Id val id: String,
        val amount: BigDecimal,
        val date: LocalDateTime,
        val accountType: AccountType,
        val customer: Customer
)

data class AccountType(
        val accountType: String,
        val name: String
)

data class Customer(
        val id: String,
        val firstName: String,
        val secondName: String,
        val lastLoginBalance: BigDecimal
)