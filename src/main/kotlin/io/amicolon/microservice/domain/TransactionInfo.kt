package io.amicolon.microservice.domain

import java.math.BigDecimal
import java.time.LocalDateTime

data class TransactionInfo(
        val transactionDate: LocalDateTime,
        val transactionId: String,
        val amount: BigDecimal,
        val accountTypeName: String,
        val customerFirstName: String,
        val customerSecondName: String
)