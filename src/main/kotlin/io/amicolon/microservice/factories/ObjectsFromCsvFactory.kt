package io.amicolon.microservice.factories

import com.github.doyaaaaaken.kotlincsv.dsl.csvReader
import io.amicolon.microservice.domain.AccountType
import io.amicolon.microservice.domain.Customer
import io.amicolon.microservice.domain.Transaction
import org.springframework.stereotype.Component
import java.io.InputStream
import java.time.LocalDateTime
import java.time.format.DateTimeFormatter
import javax.annotation.Resource

@Component
class ObjectsFromCsvFactory {

    @Resource(name = "transactions")
    private lateinit var transactionsCsv: InputStream

    @Resource(name = "customers")
    private lateinit var customersCsv: InputStream

    @Resource(name = "accounttypes")
    private lateinit var accountTypesCsv: InputStream

    private val customDateTimeFormatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss")

    fun getTransactions(): List<Transaction> {
        val customers = getDomainObjectsList(customersCsv, this::newCustomer)
        val accountTypes = getDomainObjectsList(accountTypesCsv, this::newAccountType)

        return getTransactions(customers, accountTypes)
    }

    private fun <R> getDomainObjectsList(inputStream: InputStream, mapper: (row: List<String>) -> R): List<R> {

        return csvAsSequence(inputStream)
                .drop(1)
                .map { mapper.invoke(it) }
                .toList()
    }

    private fun csvAsSequence(inputStream: InputStream) = csvReader().readAll(inputStream).asSequence()

    private fun newCustomer(row: List<String>): Customer = Customer(
            id = row[0],
            firstName = row[1],
            secondName = row[2],
            lastLoginBalance = parseBigDecimal(row[3])
    )

    private fun parseBigDecimal(amount: String) = amount.replace(",", ".").toBigDecimal()

    private fun newAccountType(row: List<String>): AccountType = AccountType(
            accountType = row[0],
            name = row[1]
    )

    private fun getTransactions(customers: List<Customer>, accountTypes: List<AccountType>): List<Transaction> {
        return csvAsSequence(transactionsCsv)
                .drop(1)
                .map { newTransaction(it, customers, accountTypes) }
                .toList()
    }

    private fun newTransaction(row: List<String>, customers: List<Customer>, accountTypes: List<AccountType>): Transaction = Transaction(
            id = row[0],
            amount = parseBigDecimal(row[1]),
            accountType = accountTypes.find { it.accountType == row[2] }!!,
            customer = customers.find { it.id == row[3] }!!,
            date = LocalDateTime.parse(row[4], customDateTimeFormatter)
    )

}