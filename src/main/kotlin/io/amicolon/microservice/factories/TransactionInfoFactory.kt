package io.amicolon.microservice.factories

import io.amicolon.microservice.domain.Transaction
import io.amicolon.microservice.domain.TransactionInfo
import org.springframework.stereotype.Component

@Component
class TransactionInfoFactory {

    fun newTransactionInfo(transaction: Transaction): TransactionInfo {
        return TransactionInfo(
                transactionId = transaction.id,
                transactionDate = transaction.date,
                amount = transaction.amount,
                accountTypeName = transaction.accountType.name,
                customerFirstName = transaction.customer.firstName,
                customerSecondName = transaction.customer.secondName
        )
    }
}