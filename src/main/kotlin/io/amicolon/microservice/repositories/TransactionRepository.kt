package io.amicolon.microservice.repositories

import io.amicolon.microservice.domain.Transaction
import org.springframework.data.mongodb.repository.MongoRepository
import org.springframework.stereotype.Repository

@Repository
interface TransactionRepository : MongoRepository<Transaction, String> {

    fun findAllByCustomer_IdInAndAccountType_AccountTypeIn(customersId: List<String>, accountType: List<String>): List<Transaction>

    fun findAllByCustomer_IdIn(customersId: List<String>): List<Transaction>

    fun findAllByAccountType_AccountTypeIn(accountTypes: List<String>): List<Transaction>

}