package io.amicolon.microservice.services

import io.amicolon.microservice.domain.TransactionInfo

interface TransactionsInfoService {
    fun obtainTransactionsInformation(customerIds: List<String>?, accountTypesIds: List<String>?): List<TransactionInfo>
}