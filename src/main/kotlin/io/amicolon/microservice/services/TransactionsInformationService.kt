package io.amicolon.microservice.services

import io.amicolon.microservice.domain.Transaction
import io.amicolon.microservice.domain.TransactionInfo
import io.amicolon.microservice.factories.TransactionInfoFactory
import io.amicolon.microservice.repositories.TransactionRepository
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service

@Service
class TransactionsInformationService @Autowired constructor(
        private val transactionInfoFactory: TransactionInfoFactory,
        private val transactionRepository: TransactionRepository
) : TransactionsInfoService {

    override fun obtainTransactionsInformation(customerIds: List<String>?, accountTypesIds: List<String>?): List<TransactionInfo> {
        val transactions = retrieveTransactions(customerIds, accountTypesIds)
        return getTransactionsInfo(transactions)
    }

    private fun retrieveTransactions(customerIds: List<String>?, accountTypesIds: List<String>?): List<Transaction> {
        return when {
            containsAllIds(customerIds) && containsAllIds(accountTypesIds) -> transactionRepository.findAll()
            containsAllIds(customerIds) -> transactionRepository.findAllByAccountType_AccountTypeIn(accountTypesIds!!)
            containsAllIds(accountTypesIds) -> transactionRepository.findAllByCustomer_IdIn(customerIds!!)
            else -> transactionRepository.findAllByCustomer_IdInAndAccountType_AccountTypeIn(customerIds!!, accountTypesIds!!)
        }
    }

    private fun containsAllIds(ids: List<String>?) = ids.isNullOrEmpty() || ids.contains("ALL")

    private fun getTransactionsInfo(transactions: Collection<Transaction>): List<TransactionInfo> {
        return transactions.asSequence()
                .map(transactionInfoFactory::newTransactionInfo)
                .sortedBy { it.amount }
                .toList()
    }
}