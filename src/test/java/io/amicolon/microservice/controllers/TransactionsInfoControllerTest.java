package io.amicolon.microservice.controllers;

import io.amicolon.microservice.domain.TransactionInfo;
import io.amicolon.microservice.services.TransactionsInfoService;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.boot.test.mock.mockito.SpyBean;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;

import java.util.List;

import static java.util.Collections.emptyList;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.*;
import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.httpBasic;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@ExtendWith(SpringExtension.class)
@WebMvcTest(controllers = TransactionsInfoController.class)
class TransactionsInfoControllerTest
{
    @Autowired
    private MockMvc mockMvc;

    @SpyBean
    private TransactionsInfoController transactionsInfoController;

    @MockBean
    private TransactionsInfoService transactionsInfoService;

    @Test
    @DisplayName("customer_id=1,3,4 and account_type=1,3")
    @WithMockUser(value = "usr1", password = "pwd1")
    public void shouldReturnDataForNonEmptyCustomerIdsAndNonEmptyAccountTypes() throws Exception
    {
        var customerIds = List.of("1", "3", "4");
        var accountTypeIds = List.of("1", "3");
        var info = List.of(mock(TransactionInfo.class), mock(TransactionInfo.class));

        when(transactionsInfoService.obtainTransactionsInformation(eq(customerIds), eq(accountTypeIds)))
                .thenReturn(info);

        mockMvc.perform(get("/api/transactions/information")
                .param("customer_id", "1", "3", "4")
                .param("account_type", "1", "3")
                .with(httpBasic("usr1", "pwd1"))
        )
                .andDo(print())
                .andExpect(status().isOk());

        verify(transactionsInfoController, times(1)).ok(info);
    }

    @Test
    @DisplayName("customer_id='' and account_type=''")
    @WithMockUser(value = "usr1", password = "pwd1")
    public void shouldReturnDataForEmptyCustomerIdsAndEmptyAccountTypes() throws Exception
    {
        var info = List.of(mock(TransactionInfo.class), mock(TransactionInfo.class), mock(TransactionInfo.class), mock(TransactionInfo.class));

        when(transactionsInfoService.obtainTransactionsInformation(eq(null), eq(null)))
                .thenReturn(info);

        mockMvc.perform(
                get("/api/transactions/information")
                        .with(httpBasic("usr1", "pwd1"))
        )
                .andDo(print())
                .andExpect(status().isOk());

        verify(transactionsInfoController, times(1)).ok(info);
    }

    @Test
    @DisplayName("customer_id=ALL and account_type=ALL")
    @WithMockUser(value = "usr2", password = "pwd2")
    public void shouldReturnDataForAllCustomerIdsAndAllAccountTypes() throws Exception
    {
        var customerIds = List.of("ALL");
        var accountTypeIds = List.of("ALL");
        var info = List.of(mock(TransactionInfo.class), mock(TransactionInfo.class), mock(TransactionInfo.class), mock(TransactionInfo.class));

        when(transactionsInfoService.obtainTransactionsInformation(eq(customerIds), eq(accountTypeIds)))
                .thenReturn(info);

        mockMvc.perform(
                get("/api/transactions/information")
                        .param("customer_id", "ALL")
                        .param("account_type", "ALL")
                        .with(httpBasic("usr1", "pwd1"))
        )
                .andDo(print())
                .andExpect(status().isOk());

        verify(transactionsInfoController, times(1)).ok(info);
    }

    @Test
    @DisplayName("customer_id=invalid and account_type=invalid")
    @WithMockUser(value = "usr3", password = "pwd3")
    public void shouldReturnEmptyListForInvalidCustomerIdsOrInvalidAccountTypes() throws Exception
    {
        var customerIds = List.of("993", "897");
        var accountTypeIds = List.of("98", "321");
        List<TransactionInfo> info = emptyList();

        when(transactionsInfoService.obtainTransactionsInformation(eq(customerIds), eq(accountTypeIds)))
                .thenReturn(info);

        MvcResult result = mockMvc.perform(
                get("/api/transactions/information")
                        .param("customer_id", "993", "897")
                        .param("account_type", "98", "321")
                        .with(httpBasic("usr1", "pwd1"))
        )
                .andDo(print())
                .andExpect(status().isOk())
                .andReturn();

        verify(transactionsInfoController, times(1)).ok(info);
        assertEquals("[]", result.getResponse().getContentAsString());
    }

    @Test
    @DisplayName("401 Status Code")
    public void shouldReturnNonAuthorizedStatusCode() throws Exception
    {
        mockMvc.perform(
                get("/api/transactions/information")
                        .param("customer_id", "all")
                        .param("account_type", "all")
                        .with(httpBasic("invalid_user", "invalid_password"))
        )
                .andDo(print())
                .andExpect(status().isUnauthorized())
                .andExpect(content().string("HTTP Status 401 - Bad credentials"));
    }

}
